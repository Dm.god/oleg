import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.9
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Олег Пойлов")

    readonly property int dpi: Screen.pixelDensity * 25.4
    function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }

    header:
        //Application Bar
        Rectangle {
        id: menuRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: dp(48)
        color: "red"
        Text{
            id: textbar
            text:
            {
                if(swipeView.currentIndex == 0){qsTr("Авторизация")}
                if(swipeView.currentIndex == 1){qsTr("Пользователь: " + edtLogin1.text)}
                if(swipeView.currentIndex == 2){qsTr("Информация")}
            }
            font.bold: true;
            font.pixelSize: 20
            color: "white"
            anchors{
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
        }
        Rectangle {
            id: navPanel
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            visible: false
            width: dp(48)
            color: "red"
            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(16)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(23)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(30)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    nav.toggle()
                }
            }
        }
    }


    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        interactive: false3
        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent;
                    Rectangle {
                        Layout.preferredWidth:  70 * Screen.pixelDensity
                        Layout.preferredHeight: 40 * Screen.pixelDensity
                        Layout.alignment: Qt.AlignCenter
                        ColumnLayout {
                            anchors.centerIn: parent;
                            width:50 * Screen.pixelDensity
                            height:50 * Screen.pixelDensity


                            TextField {
                                    id: edtLogin1
                                    font.pointSize: 10
                                    placeholderText: "Логин"
                                    Layout.preferredWidth:  40 * Screen.pixelDensity
                                    Layout.preferredHeight: 10 * Screen.pixelDensity
                                    Layout.alignment: Qt.AlignCenter
                            }
                            TextField {
                                    id: edtPassword1
                                    Layout.preferredWidth:  40 * Screen.pixelDensity
                                    Layout.preferredHeight: 10 * Screen.pixelDensity
                                    Layout.alignment: Qt.AlignCenter
                                    font.pointSize: 10
                                    echoMode: "Password"
                                    placeholderText: "Пароль"
                            }
                            Button {
                                id: btnAuth
                                text: "Вход"
                                font.pointSize: 18
                                Layout.preferredWidth:  40 * Screen.pixelDensity
                                Layout.preferredHeight: 10 * Screen.pixelDensity
                                Layout.alignment: Qt.AlignCenter
                                onClicked: {
                                    if (edtLogin1.text == "Олег" && edtPassword1.text == "1234") {
                                        nav.visible = true
                                        navPanel.visible = true
                                        tabBar.visible = true
                                        edtPassword1.clear()
                                        swipeView.interactive = true
                                        swipeView.setCurrentIndex(swipeView.currentIndex + 1)
                                    }
                                }
                            }
                        }
                   }
            }
        }

        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent;

                    Button {
                        id: btnReq
                        text: "Запрос"
                        font.pointSize: 18
                        Layout.row: 1
                        Layout.preferredWidth:  40 * Screen.pixelDensity
                        Layout.preferredHeight: 10 * Screen.pixelDensity
                        Layout.alignment: Qt.AlignCenter
                    }
                }
        }

        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent


                    Rectangle {
                        id: logo1
                        Layout.preferredWidth:  40 * Screen.pixelDensity
                        Layout.preferredHeight: 40 * Screen.pixelDensity
                        Layout.row: 1
                        Layout.alignment: Qt.AlignCenter
                        Image {
                            id: mosPol1
                            anchors.fill: parent
                            source: "./logo.jpg"
                        }
                    }

                    Text {
                        font.pointSize: 8
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Программа нужна для \n отправки запросов на сервер\nполучения ответа\nи отображения на экране"
                    }

                }
        }

    }


    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        visible: false
        TabButton {
            text: qsTr("авторизация")
        }
        TabButton {
            text: qsTr("запрос")
        }
        TabButton {
            text: qsTr("информация")
        }
    }



    NavigationDrawer {
        id: nav
        anchors.bottom: parent.bottom
        visible: false
        position: Qt.LeftEdge
        //visualParent: stackView
        Rectangle {
            anchors.fill: parent
            color: "red"
            ListView{
                id: lstPagesMenu
                model: navModel
                anchors.fill: parent
                anchors.margins: 10
                delegate: Button
                {
                    text: stranica
                    width: lstPagesMenu.width
                    height:Screen.pixelDensity * 12
                    onClicked:
                    {
                        swipeView.currentIndex = index
                    }
                }
            }
        }
    }
    ListModel {
        id: navModel

        ListElement {stranica: "Авторизация"}
        ListElement {stranica: "Запрос"}
        ListElement {stranica: "Информация"}
    }
}
